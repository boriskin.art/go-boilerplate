# Build binary with downloaded go modules
FROM golang:1.19-buster as build

# Copy files
RUN mkdir /home/planitpoker
COPY ./go.mod /home/planitpoker
WORKDIR /home/planitpoker

# Download go modules to cache
RUN go mod download
COPY . /home/planitpoker

# Build application
RUN CGO_ENABLED=0 go build -o planitpoker ./cmd/service/*

# Start app in low-size alpine image
FROM alpine:3.14.1

RUN mkdir planitpoker

# Copy files from build step
WORKDIR planitpoker
COPY --from=build /home/planitpoker/planitpoker .
RUN mkdir config
COPY --from=build /home/planitpoker/internal/config ./config

# Start app
CMD ["./planitpoker"]


