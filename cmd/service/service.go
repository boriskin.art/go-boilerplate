package main

import (
	"context"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"

	"go-boilerplate/internal/config"
	"go-boilerplate/internal/db"
	"go-boilerplate/internal/mwares"
	"go-boilerplate/internal/utils"
	"go-boilerplate/pkg/logger"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	log, err := logger.New()
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}

	ctx = logger.ToContext(ctx, log)

	defer func() {
		if panicErr := recover(); panicErr != nil {
			utils.LogAndExit(ctx, err)
		}
		if err != nil {
			utils.LogAndExit(ctx, err)
		}
	}()

	// Config
	cfg := config.MustInitConfig()

	// DB
	appDb := db.MustInitDB(cfg)
	_ = appDb

	router := mux.NewRouter()

	// Middlewares
	mws := []mux.MiddlewareFunc{
		mwares.New(ctx).ContextMW,
	}

	router.Use(mws...)

	srv := &http.Server{
		Handler:      router,
		Addr:         "127.0.0.1:8080",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	utils.LogAndExit(ctx, srv.ListenAndServe())
}
