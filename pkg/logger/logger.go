package logger

import (
	"context"

	"go.uber.org/zap"
)

type key int

const ctxKey key = 1

type Logger interface {
	Info(args ...interface{})
	Debug(args ...interface{})
	Warn(args ...interface{})
	Error(args ...interface{})
	Fatal(args ...interface{})
}

func New() (Logger, error) {
	log, err := zap.NewDevelopment()
	if err != nil {
		return nil, err
	}
	defer func() { _ = log.Sync() }()

	return log.Sugar(), nil
}

func ToContext(ctx context.Context, logger Logger) context.Context {
	return context.WithValue(ctx, ctxKey, logger)
}

func FromContext(ctx context.Context) Logger {
	return ctx.Value(ctxKey).(Logger)
}
