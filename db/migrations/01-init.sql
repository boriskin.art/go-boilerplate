create table if not exists room (
    id bigserial primary key,

    name text not null,
    user_id int not null, -- id создателя комнаты
    active_issue_id int default null,

    created_at timestamptz default now(),
    updated_at timestamptz default now()
);

create table if not exists issue (
    id bigserial primary key,

    name text not null,
    room_id int not null,
    estimate int default null, -- оценка задачи

    created_at timestamptz default now(),
    updated_at timestamptz default now()
);

create table if not exists vote (
    id bigserial primary key,

    issue_id int not null,
    user_id int not null,
    vote int not null,

    created_at timestamptz default now()
);