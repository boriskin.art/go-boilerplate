package mwares

import (
	"context"
	"net/http"
)

type MW struct {
	ctx context.Context
}

func New(ctx context.Context) MW {
	return MW{ctx: ctx}
}

func (mw MW) ContextMW(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r.WithContext(mw.ctx))
	})
}
