package db

import (
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"go-boilerplate/internal/config"
)

func MustInitDB(cfg config.Config) *gorm.DB {
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable",
		cfg.DataBaseConfig.Host,
		cfg.DataBaseConfig.User,
		cfg.DataBaseConfig.Password,
		cfg.DataBaseConfig.DbName,
		cfg.DataBaseConfig.Port,
	)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("error while init database connection")
	}
	return db
}
