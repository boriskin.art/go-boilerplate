package config

func MustInitConfig() Config {
	cfg := initDefaultConfig()

	return cfg
}

type Config struct {
	DataBaseConfig DataBaseConfig
}

type DataBaseConfig struct {
	Host     string
	User     string
	Password string
	DbName   string
	Port     string
}

func initDefaultConfig() Config {
	return Config{
		DataBaseConfig: DataBaseConfig{
			Host:     "localhost",
			User:     "postgres",
			Password: "1234",
			DbName:   "postgres",
			Port:     "5432",
		},
	}
}
