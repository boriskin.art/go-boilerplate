package utils

import (
	"context"
	"go-boilerplate/pkg/logger"
	"os"
)

func LogAndExit(ctx context.Context, err error) {
	log := logger.FromContext(ctx)
	log.Error("error while start service", err)
	os.Exit(1)
}
