package utils

import "time"

func TimeToString(t time.Time) string {
	return t.Format(time.RFC3339)
}
